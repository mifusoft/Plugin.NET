﻿namespace PluginNET.events
{
    /// <summary>
    /// 创建插件中的类实例后的事件数据
    /// </summary>
    public class PluginInstanceCreatedArgs<T> : PluginInstanceCreatingArgs where T : class
    {
        /// <summary>
        /// 插件类的实例对象
        /// </summary>
        public T Instance { get; set; }
    }
}
